module Main where

import Evakuil (evakuiViki, analizPonto, konekti, konservujTraktil, noForumArg, proxyArg, threadsArg, websiteArg)
import RIO
import System.Console.CmdArgs

data WikidotEvakuil = WikidotEvakuil
  { website :: Text,
    threads :: Int,
    proxy :: Maybe Text,
    noForum :: Bool
  }
  deriving (Data, Typeable, Show)

main :: IO ()
main = do
  arg <-
    cmdArgs $
      WikidotEvakuil
        { website = websiteArg,
          threads = threadsArg,
          proxy = proxyArg,
          noForum = noForumArg
        }
        &= program "wikidot-evakuilo"
  ponto <- traverse analizPonto $ proxy arg
  konekto <- konekti ponto
  evakuiViki
    (konservujTraktil $ website arg)
    konekto
    (website arg)
    (threads arg)
    (noForum arg)
