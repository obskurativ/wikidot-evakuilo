{ mkDerivation, aeson, ansi-terminal, async, base, bytestring
, cmdargs, directory, filepath, freer-simple, http-client
, http-client-tls, http-types, lib, regex-tdfa, rio, scalpel, stm
, stm-containers, text
}:
mkDerivation {
  pname = "wikidot-evakuilo";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson ansi-terminal async base bytestring cmdargs directory
    filepath freer-simple http-client http-client-tls http-types
    regex-tdfa rio scalpel stm stm-containers text
  ];
  executableHaskellDepends = [
    aeson ansi-terminal async base bytestring cmdargs directory
    filepath freer-simple http-client http-client-tls http-types
    regex-tdfa rio scalpel stm stm-containers text
  ];
  license = lib.licenses.mit;
  mainProgram = "wikidot-evakuilo";
}
