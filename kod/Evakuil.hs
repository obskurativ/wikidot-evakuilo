module Evakuil (analizPonto, konekti, evakuiViki, evakuiPagx, konservujTraktil, KonservujTrk, Konservujo (..), ArtikolMeta (..), ArtikolVer (..), WikidotKonekt, websiteArg, threadsArg, proxyArg, noForumArg) where

import Control.Monad.Freer
import Control.Monad.Freer.Error
import Control.Monad.Freer.Reader
import Control.Monad.Freer.Writer
import Data.Aeson
import qualified Data.ByteString.Lazy as BSL
import qualified Data.ByteString.Char8 as BSC
import Data.Monoid (Any (..))
import qualified Data.Text.IO as T
import Efekt
import Network.HTTP.Client hiding (proxy)
import Network.HTTP.Client.TLS
import Network.HTTP.Types (statusCode, statusMessage)
import Network.HTTP.Types.Header (hContentType, hUserAgent)
import RIO hiding (Proxy, Reader, ask, asks, runReader)
import qualified RIO.List as L
import qualified RIO.List.Partial as LP
import qualified RIO.Partial as P
import qualified RIO.Text as T
import qualified RIO.Text.Partial as TP
import RIO.Time
import qualified StmContainers.Set as S
import System.Console.CmdArgs
import System.Directory
import System.FilePath
import Text.HTML.Scalpel
import Text.Regex.TDFA

hush :: Either l d -> Maybe d
hush (Right d) = Just d
hush (Left _) = Nothing

websiteArg :: Text
websiteArg = "" &= argPos 0 &= typ "SITENAME"

threadsArg :: Int
threadsArg = 1 &= opt (1 :: Int) &= help "Number of threads. Try not to overload Wikidot. Thank you!"

proxyArg :: Maybe Text
proxyArg = def &= help "Proxy to use (Example: 127.0.0.1:8118)"

noForumArg :: Bool
noForumArg = def &= help "Completely ignore the forum"

analizPonto :: Text -> IO Proxy
analizPonto x = case T.split (== ':') x of
  [un, du] -> pure $ Proxy (encodeUtf8 un) (tekstAlInt du)
  _ -> fail "I wasn't able to understand --proxy argument."

konekti :: Maybe Proxy -> IO WikidotKonekt
konekti ponto = do
  uzantnomo : pasvorto : _ <- T.lines <$> readFileUtf8 "credentials.txt"
  konektil' <- newManager $ foldr managerSetProxy tlsManagerSettings (useProxy <$> ponto)
  let auxtentPet =
        urlEncodedBody
          [ ("action", "Login2Action"),
            ("event", "login"),
            ("login", T.encodeUtf8 uzantnomo),
            ("password", T.encodeUtf8 pasvorto)
          ]
          $ evakuiloHeader
            "https://www.wikidot.com/default--flow/login__LoginPopupScreen"
  auxtentResp <- httpNoBody auxtentPet konektil'
  let kukoj'' = responseCookieJar auxtentResp
  let token7' =
        cookie_value $
          P.fromJust $
            L.find (\x -> x.cookie_name == "wikidot_token7") $
              destroyCookieJar kukoj''
  let kukoj' = \retej' ->
        createCookieJar $
          (\x -> x {cookie_domain = wikidotDomajn $ T.encodeUtf8 retej'}) <$> destroyCookieJar kukoj''
  pure $ WikidotKonekt konektil' token7' kukoj'

evakuiViki :: KonservujTrk -> WikidotKonekt -> Text -> Int -> Bool -> IO ()
evakuiViki trk konekt retej' procezoj noforum = do
  -- Krei planon
  vic' <- newTQueueIO
  atomically $ writeTQueue vic' (Art' "")
  konataj' <- S.newIO
  mortnom' <- newTVarIO procezoj
  T.putStrLn "" -- Preterpasi linio. Ĝi estos anstataŭigita far status-montrilo.
  let retdosuj = "data" </> T.unpack retej'
  createDirectoryIfMissing True retdosuj
  kunStatusTraktil (retdosuj </> "failed.txt") \status -> do
    let plan = Plan vic' konataj' mortnom' konekt retej' [wikidotDomajn retej'] False
    runM $ planMedio plan $ status do
      if noforum
        then do
          sSciigi "[inf] Reading the forum skipped"
        else do
          sSciigi "[inf] Reading the forum"
          akiriFadenojn
      sSciigi "[inf] Reading sitemap.xml"
      sitemap <-
        sekuraIO $
          httpLbs
            (petoAl retej' "sitemap.xml")
            konekt.konektil
      troviArtLig "sitemap.xml" (decodeUtf8Lenient $ BSL.toStrict $ responseBody $ P.fromJust $ hush sitemap)
      sSciigi "[inf] Initialization complete"

    -- Krei procezojn
    asyncs <- for [1 .. procezoj] \_ -> async $ laborant trk status plan
    _ <- waitAnyCancel asyncs

    pure ()

evakuiPagx :: KonservujTrk -> WikidotKonekt -> Text -> Text -> IO ()
evakuiPagx trk konekt retej' art = do
  vic' <- newTQueueIO
  atomically $ writeTQueue vic' (kreiArt art)
  konataj' <- S.newIO
  mortnom' <- newTVarIO 1
  let plan = Plan vic' konataj' mortnom' konekt retej' [wikidotDomajn retej'] True
  laborant
    trk
    ( interpret \case
        SSciigi _ -> pure ()
        SPlan _ -> pure ()
    )
    plan

data FinisLaboron = FinisLaboron

mapErar :: forall e1 e2 efj v. Member (Error e2) efj => (e1 -> e2) -> Eff (Error e1 ': efj) v -> Eff efj v
mapErar f = flip handleError (throwError . f)

atend :: Members '[STM, Error FinisLaboron, Reader Plan] efj => Eff efj Obj
atend = do
  (plan :: Plan) <- ask
  sekv <- stm $ tryReadTQueue plan.vic
  case sekv of
    Just x -> pure x
    Nothing -> do
      finis <- stm do
        mnom <- readTVar plan.mortnom
        if mnom <= 1
          then pure True
          else do
            writeTVar plan.mortnom (mnom - 1)
            pure False
      when finis (throwError FinisLaboron)
      stm do
        r <- readTQueue plan.vic
        modifyTVar plan.mortnom (+ 1)
        pure r

envici :: Members '[StatusSignal, STM, Reader Plan] efj => Obj -> Eff efj ()
envici obj = do
  plan :: Plan <- ask
  let konservinda =
        if plan.loka
          then case obj of
            Dos' _ _ _ -> True
            _ -> False
          else True
  konservu <-
    if konservinda
      then stm do
        nekonata <- not <$> S.lookup obj plan.konataj
        when nekonata $
          S.insert obj plan.konataj
        pure $ nekonata
      else pure False
  when konservu do
    stm $ writeTQueue plan.vic obj
    sPlan (\x _ -> pure $ x {sEntute = x.sEntute + 1})

wikidotDomajn :: (Monoid a, IsString a) => a -> a
wikidotDomajn retej' = retej' <> ".wikidot.com"

evakuiloHeader :: Request -> Request
evakuiloHeader x = x {requestHeaders = x.requestHeaders <> [(hUserAgent, "WikidotEvakuilo/1.0")]}

-- | @peto "blabla" "article_name" = "http://blabla.wikidot.com/article_name"@
petoAl :: Text -> Text -> Request
petoAl retej' voj =
  evakuiloHeader $ parseRequest_ $ T.unpack ("http://" <> wikidotDomajn retej' <> "/" <> voj)

data PetErar = NeTrovitaErar | AliaPetErar

peti :: Members '[Reader Plan, SekuraIO, StatusSignal, Error PetErar] efj => Request -> Eff efj (BSL.ByteString, ByteString)
peti pet = do
  plan :: Plan <- ask
  let reprovi erar = \case
        (0 :: Int) -> do
          sSciigi $ "[error] " <> tshow erar
          throwError AliaPetErar
        n -> peti' n
      peti' n = do
        (P.fromJust . hush -> nun) <- sekuraIO getCurrentTime
        respM <-
          sekuraIO $
            httpLbs
              (fst $ insertCookiesIntoRequest pet (plan.konekt.kukoj plan.retej) nun)
              plan.konekt.konektil
        case respM of
          Left erar -> reprovi erar (n - 1)
          Right resp ->
            let path' = decodeUtf8Lenient $ pet.host <> "/" <> pet.path
                status' = resp.responseStatus
                kod = status'.statusCode
             in if
                    | kod == 200 ->
                        pure
                          ( responseBody resp,
                            responseHeaders resp
                              & L.find (\x -> fst x == hContentType)
                              & P.fromJust
                              & snd
                              & BSC.takeWhile (/= ';')
                          )
                    | kod == 403 -> do
                        sSciigi $ "[error] " <> path' <> " 403 forbidden"
                        throwError AliaPetErar
                    | kod == 404 -> throwError NeTrovitaErar
                    | kod >= 400 && kod <= 499 -> do
                        sSciigi $ "[error] HTTP client error " <> path' <> " " <> tshow resp
                        throwError AliaPetErar
                    | otherwise -> reprovi (path' <> " " <> tshow kod <> " " <> decodeUtf8Lenient status'.statusMessage) (n - 1)
  peti' 3

petiAuCxes :: Members '[Reader Plan, SekuraIO, StatusSignal, Error SavCxes] efj => Request -> Eff efj (BSL.ByteString, ByteString)
petiAuCxes =
  mapErar
    ( \case
        NeTrovitaErar -> SavNeBezonata
        AliaPetErar -> SavFiasko
    )
    . peti

data SavCxes = SavNeBezonata | SavFiasko

sekuraIOAuCxes :: Members '[StatusSignal, SekuraIO, Error SavCxes] efj => IO () -> Eff efj ()
sekuraIOAuCxes f = do
  a <- sekuraIO f
  case a of
    Left erar -> do
      sSciigi $ "[error] IO: " <> erar
      throwError SavFiasko
    Right _ -> pure ()

laborant ::
  KonservujTrk ->
  (forall efj a. (LastMember IO efj, Member STM efj) => Eff (StatusSignal ': efj) a -> Eff efj a) ->
  Plan ->
  IO ()
laborant trk status plan = do
  void $
    runM $
      runError @FinisLaboron $ -- Traktilo por finilo
        interpretM trk $ -- Konservujo
          planMedio plan $
            status $
              forever do
                obj <- atend
                rez <- runError @SavCxes $
                  case obj of
                    Art' art -> artikol art
                    Dos' https retej' dos -> do
                      let protokol = if https then "https" else "http"
                      savi <-
                        konsDosieron (retej', dos) >>= \case
                          Nothing -> throwError SavNeBezonata
                          Just x -> pure x
                      p <- petiAuCxes $ parseRequest_ $ T.unpack $ protokol <> "://" <> retej' <> "/" <> dos
                      sekuraIOAuCxes $ uncurry savi p
                    Forum n1 n2 fadenId -> fadeno n1 n2 fadenId
                case rez of
                  Left SavNeBezonata -> sPlan (\x _ -> pure $ x {sEntute = x.sEntute - 1})
                  Left SavFiasko -> do
                    sPlan
                      ( \x h -> do
                          -- FARENDE: informi pri la eraro
                          T.hPutStr h $ tshow obj <> "\n"
                          pure $ x {sFiask = x.sFiask + 1}
                      )
                  Right () -> sPlan (\x _ -> pure $ x {sSukc = x.sSukc + 1})

data AjaxResp = AjaxResp {status :: Text, body :: Maybe Text} deriving (Generic)

instance FromJSON AjaxResp

ajax ::
  Members '[SekuraIO, Reader Plan, Error SavCxes, StatusSignal] efj =>
  [(ByteString, ByteString)] ->
  Eff efj Text
ajax argoj = do
  plan :: Plan <- ask
  (respond, _) <-
    mapErar @PetErar (const SavFiasko) $
      peti $
        urlEncodedBody
          (argoj <> [("callbackIndex", "0"), ("wikidot_token7", plan.konekt.token7)])
          (petoAl plan.retej "ajax-module-connector.php")
  case decode' respond of
    Just (AjaxResp {status = "ok", body = Just r'}) -> pure r'
    Just (AjaxResp {status = "no_permission"}) -> throwError SavFiasko -- Gracia eraro, ne estas priavertita
    _ -> do
      sSciigi $ "[error] Unable to parse JSON:" <> decodeUtf8Lenient (BSL.toStrict respond)
      throwError SavFiasko

tekstAlInt :: Text -> Int
tekstAlInt = fromMaybe 0 . strAlInt 0 . T.unpack
  where
    strAlInt n = \case
      (xchr : xs) -> chrAlNum xchr >>= \x -> strAlInt (10 * n + x) xs
      [] -> Just n

artikol ::
  Members '[SekuraIO, Reader Plan, Error SavCxes, Konservujo, STM, StatusSignal] efj =>
  Text ->
  Eff efj ()
artikol art = do
  plan :: Plan <- ask
  pagxo <- decodeUtf8Lenient . BSL.toStrict . fst <$> petiAuCxes (petoAl plan.retej art)
  unless plan.loka $ troviArtLig art pagxo
  pagxId <-
    case pagxo `regex` "WIKIREQUEST\\.info\\.pageId = ([[:digit:]]+)" of
      (_ : x : _) : _ -> pure x
      _ -> do
        sSciigi $ "[error] Page " <> art <> " doesn't contain page id"
        throwError SavFiasko
  let anstTabel =
        [ ("&lt;", "<"),
          ("&gt;", ">"),
          ("&amp;", "&"),
          ("&quot;", "\""),
          ("&#039;", "\'"),
          ("&nbsp;", " "),
          ("<br />", "")
        ]
      anst str = anst' str anstTabel
      anst' str = \case
        (pref, al) : rs
          | Just xs <- L.stripPrefix pref str -> al <> anst xs
          | otherwise -> anst' str rs
        []
          | (x : xs) <- str -> x : anst xs
          | [] <- str -> []
      pagxTitol =
        case pagxo `regex` "<div id=\"page-title\">\n([^<]+)" of
          (_ : x : _) : _ -> Just $ T.pack $ anst $ T.unpack $ T.strip x
          _ -> Nothing
      pagxEtj = P.fromJust $ scrapeStringLike pagxo $ texts ("div" @: [hasClass "page-tags"] // "span" // "a")
  (lasteKonataVersio, savi) <-
    konsArtikolon
      ( case TP.splitOn ":" art of
          [a, b] -> (Just a, b)
          _ -> (Nothing, art)
          & \case
            (a, "") -> (a, Nothing)
            (a, b) -> (a, Just b)
      )
      ArtikolMeta
        { artTitol = pagxTitol,
          artEtikedoj = pagxEtj
        }

  let akirKrudVersioj (n :: Int) = do
        resp <-
          ajax
            [ ("moduleName", "history/PageRevisionListModule"),
              ("options", "{\"all\":true}"),
              ("page_id", encodeUtf8 pagxId),
              ("page", encodeUtf8 $ tshow n),
              ("perpage", "100")
            ]
        let rez = P.fromJust $ scrapeStringLike resp do
              chroots "tr" do
                p <- position
                guard (p /= 0)
                kod <- attr "id" "input"
                do
                  inSerial do
                    (T.strip >>> T.takeWhile (/= '.') >>> tekstAlInt -> ver) <- seekNext (text "td")
                    verkint <- seekNext (text $ "td" // "span" @: [hasClass "printuser"])
                    pure (kod, ver, verkint)
        if length rez == 100
          then (rez <>) <$> akirKrudVersioj (n + 1)
          else pure rez

  krudVersioj <- akirKrudVersioj 1

  let filtrVer en1 =
        let (r1, en2) = L.splitAt 2 en1
            (r2, _) = L.splitAt 3 $ reverse en2
         in r1 <> r2
      versioj = filtrVer $ takeWhile (\(_, versio, _) -> versio > lasteKonataVersio) krudVersioj
  ((), Any iuFiaskis) <- runWriter $ forM_
    versioj
    \(sxangxId, versio, verkint) -> (`catchError` \(_ :: SavCxes) -> tell $ Any True) do
      pagxfontKruda <-
        ajax
          [ ("moduleName", "history/PageSourceModule"),
            ("revision_id", encodeUtf8 sxangxId)
          ]
      let forigiKomenc = \case
            str | Just xs <- L.stripPrefix "<div class=\"page-source\">\n" str -> xs
            _ : xs -> forigiKomenc xs
            [] -> undefined
          pagxfont = T.pack $ L.dropSuffix "</div>" $ anst $ forigiKomenc $ T.unpack pagxfontKruda
      sekuraIOAuCxes $
        savi $
          ArtikolVer
            { verId = versio,
              verEnhav = pagxfont,
              verVerkint = verkint
            }
      troviArtLig art pagxfont

  when (L.length versioj > 0) do
    when plan.loka $ troviArtLig art pagxo
    -- Envici dosierojn de la artikolo
    dosoj <-
      ajax
        [ ("moduleName", "files/PageFilesModule"),
          ("page_id", encodeUtf8 pagxId)
        ]
        <&> \v ->
          kreiDosUnc . (False,wikidotDomajn plan.retej,) <$> v `subkong1` "\\/(local--files\\/[^\"]+)"
    forM_ dosoj envici
  when iuFiaskis $
    throwError SavFiasko

regex :: RegexLike Regex a => a -> ByteString -> [[a]]
regex = (=~)

-- Akiri unuan subkongruon
subkong1 :: RegexLike Regex a => a -> ByteString -> [a]
subkong1 v r =
  v `regex` r <&> \case
    (_ : a : _) -> a
    _ -> error "wrong regex"

troviArtLig :: Members '[Reader Plan, StatusSignal, STM] eff => Text -> Text -> Eff eff ()
troviArtLig adres enhavo = do
  -- Cxi-funkcie ni sercxas ligilojn al aliaj artikoloj kaj alpinglitaj dosieroj.
  -- Ni NE sercxas ligilojn al forumfadenoj. Ĉi tio sensencas.
  permesatajRetejoj <- asks retejNomoj
  let (art', dos', kv) =
        let tekstElem l t = l `elem` T.unpack t
            plenvojaj =
              [ (protokol == "https", retej', voj)
                | [_, protokol, retej', _, voj] <- enhavo `regex` "(https?):\\/\\/([^\\/ ]+)(\\/([A-Za-z0-9_~.:\\/@$&'*\\+;%=-]*))?"
              ]
            -- Dividi plenvojaj ligiloj inter dosieraj kaj nedosieraj ligiloj.
            (wd, neWd) = L.partition (\(_, _, voj') -> "local--files/" `T.isPrefixOf` voj') plenvojaj
            (dosaj, nedosaj) = L.partition (\(_, _, voj) -> '.' `tekstElem` voj) neWd
            (art, _) = L.partition (\(_, ret, _) -> ret `elem` permesatajRetejoj) nedosaj
         in ( (LP.head . TP.splitOn "/" . \(_, _, voj) -> voj) <$> art,
              wd <> dosaj,
              length plenvojaj
            )

  let ligKv = length $ enhavo `regex` "https?:\\/\\/"
  when (ligKv /= kv) $
    sSciigi $
      "[warning] Located "
        <> tshow kv
        <> " links on page "
        <> adres
        <> ", but additional checks indicated presence of "
        <> tshow ligKv
        <> " links. This is possibly a bug."
  let art =
        art'
          <> (subkong1 enhavo "\\[\\[include :?([^]]+)\\]\\]")
          <> (subkong1 enhavo "\\[\\[\\[ ?([A-Za-z0-9:-]+)")
  forM_
    ((kreiArt <$> art) <> (kreiDosUnc <$> dos'))
    envici

skrapiPagxon ::
  Members '[SekuraIO, Error SavCxes, StatusSignal, Reader Plan] efj =>
  Text ->
  ScraperT Text (Eff efj) a ->
  Eff efj (Maybe a)
skrapiPagxon nom skrapil = do
  plan :: Plan <- ask
  pagx <- decodeUtf8Lenient . BSL.toStrict . fst <$> petiAuCxes (petoAl plan.retej nom)
  scrapeStringLikeT pagx skrapil

akiriFadenojn :: Members '[SekuraIO, STM, StatusSignal, Reader Plan] efj => Eff efj ()
akiriFadenojn = flip (handleError @SavCxes) (\_ -> sSciigi "[fatal] Unable to read forum") do
  let kat klas adr =
        skrapiPagxon adr $
          chroots ("div" @: [hasClass klas] // "div" @: [hasClass "title"]) $
            (,) <$> text "a" <*> (T.dropPrefix "/" <$> attr "href" "a")
  sktj <-
    kat "forum-start-box" "forum:start" >>= \case
      Just x -> pure x
      Nothing -> throwError SavFiasko
  forM_ sktj \(sktNom, T.dropWhileEnd (/= '/') -> sktAdr) ->
    let skt prevFiask pagx = do
          fadenojM <- kat "forum-category-box" (sktAdr <> "p/" <> tshow pagx)
          case fadenojM of
            Just fadenoj -> do
              forM_ fadenoj \(fadenNom, fadenLig) ->
                case fadenLig `regex` "forum\\/t-([[:digit:]]+)" of
                  [[_, tekstAlInt -> fadenId]] -> envici $ Forum sktNom fadenNom fadenId
                  _ -> sSciigi $ "[error] Unable to parse link " <> fadenLig
              when (not $ null fadenoj) $
                skt False $
                  pagx + 1
            Nothing -> do
              if prevFiask
                then throwError SavFiasko
                else skt True $ pagx + 1
     in skt False (1 :: Int)

fadeno :: Members '[SekuraIO, Konservujo, Reader Plan, StatusSignal, Error SavCxes, STM] efj => Text -> Text -> Int -> Eff efj ()
fadeno nom1 nom2 fadenId = do
  savi <- konsFadenon (nom1, nom2)
  let pagx p = do
        resp <-
          ajax
            [ ("moduleName", "forum/ForumViewThreadPostsModule"),
              ("pageNo", encodeUtf8 $ tshow p),
              ("order", ""),
              ("t", encodeUtf8 $ tshow fadenId)
            ]
        skrap <- scrapeStringLikeT ("<div>" <> resp <> "</div>") (fadenmsg [p])
        case skrap of
          Nothing -> do
            sSciigi $ "[error] Unable to scrape the following page: " <> resp
            throwError SavFiasko
          Just 0 -> pure ()
          Just _ -> pagx $ p + 1
      fadenmsg revVoj = do
        void $
          optional $ chroot ("div" @: [hasClass "post"] `atDepth` 1) do
            let headS = "div" @: [hasClass "head"]
            title <- text $ headS // "div" @: [hasClass "title"]
            _ : author : _ <- texts $ headS // "div" @: [hasClass "info"] // "a"
            date <- text $ headS // "span" @: [hasClass "odate"]
            content <- innerHTML $ "div" @: [hasClass "content"]
            lift $
              (`handleError` \(_ :: SavCxes) -> tell $ Any True) $
                sekuraIOAuCxes $
                  savi revVoj $
                    "Title: "
                      <> T.strip title
                      <> "\n"
                      <> "Author: "
                      <> author
                      <> "\n"
                      <> "Date: "
                      <> date
                      <> "\n"
                      <> T.strip content
                      <> "\n"
            lift $ troviArtLig "(post message)" content
        length <$> chroots ("div" @: [hasClass "post-container"] `atDepth` 1) do
          pos <- position
          fadenmsg (pos : revVoj)
  ((), Any iuFiaskis) <- runWriter $ pagx (1 :: Int)
  when iuFiaskis $
    throwError SavFiasko
