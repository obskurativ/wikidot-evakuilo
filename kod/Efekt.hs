module Efekt where

import Control.Monad.Freer
import Control.Monad.Freer.Reader
import Control.Monad.Freer.TH
import Control.Monad.STM
import qualified Data.Text as T
import Data.Text.IO
import Network.HTTP.Client
import Network.HTTP.Types (urlDecode)
import RIO hiding (Reader, ask, asks, runReader)
import qualified RIO.ByteString.Lazy as BSL
import qualified RIO.List as L
import qualified RIO.List.Partial as LP
import qualified RIO.Text.Partial as TP
import qualified StmContainers.Set as S
import System.Console.ANSI
import System.Directory
import System.FilePath
import System.IO (hSetEncoding, utf8_bom)

data ArtikolMeta = ArtikolMeta
  { artTitol :: !(Maybe Text),
    artEtikedoj :: ![Text]
  }

data ArtikolVer = ArtikolVer
  { verId :: !Int,
    verEnhav :: !Text,
    verVerkint :: !Text
  }

data Konservujo a where
  -- | (Kategorio de pagxo, nomo de pagxo) -> ArtikolMeta  -> (versio de lasta konata pagxo, versio de pagxo -> tekst -> IO ())
  KonsArtikolon :: (Maybe Text, Maybe Text) -> ArtikolMeta -> Konservujo (Int, ArtikolVer -> IO ())
  -- | (Retejo, vojo al la dosiero) -> (Maybe (enmetinda datumo -> tipo de la datumo -> IO ()))
  KonsDosieron :: (Text, Text) -> Konservujo (Maybe (BSL.ByteString -> ByteString -> IO ()))
  -- | (Subkategorio, nomo de fadeno) -> (voj al msg ene de fadeno -> enhavo de msg -> IO ())
  KonsFadenon :: (Text, Text) -> Konservujo ([Int] -> Text -> IO ())

type KonservujTrk = forall a. Konservujo a -> IO a

konservujTraktil :: Text -> KonservujTrk
konservujTraktil retej' (KonsArtikolon (kat, dos) meta) = do
  let dataDosuj = "data" </> T.unpack retej'
      katDosuj = foldl' (</>) dataDosuj (T.unpack <$> kat)
      artikolDosuj = katDosuj </> maybe "main" T.unpack dos
  createDirectoryIfMissing True artikolDosuj
  dosUtf8 (artikolDosuj </> "meta.txt") $
    T.intercalate "\n" (toList (artTitol meta) <> [T.intercalate " " (artEtikedoj meta)]) <> "\n"
  versioj <- listDirectory artikolDosuj
  let lasta = foldl' max (-1) do
        versio <- versioj
        versioAlInt versio 0
  pure
    ( lasta,
      \ver ->
        let dosEnhav = T.intercalate "\n" [verVerkint ver, "----", verEnhav ver]
         in dosUtf8 (artikolDosuj </> (show (verId ver) <> ".txt")) dosEnhav
    )
  where
    versioAlInt :: FilePath -> Int -> [Int]
    versioAlInt ((chrAlNum -> Just x) : xs) n =
      versioAlInt xs (10 * n + x)
    versioAlInt ".txt" n = [n]
    versioAlInt _ _ = []
konservujTraktil retej' (KonsDosieron (dosRetej, disExt -> (dosNomPre, dosNomPost))) = do
  let dosuj = "data" </> T.unpack retej' </> "FILES"
      voj = dosuj </> (kreiNomSenExt <> T.unpack dosNomPost)
  createDirectoryIfMissing True dosuj
  i <- doesFileExist voj
  pure $
    if not i
      then Just $ \enhav _ -> BSL.writeFile voj enhav
      else Nothing
  where
    kreiNomSenExt =
      L.intercalate "_" $
        T.unpack
          <$> reverse (L.filter (/= "local--files") $ TP.splitOn "/" dosNomPre)
            <> reverse (LP.init $ TP.splitOn "." dosRetej)
konservujTraktil retej' (KonsFadenon (T.unpack -> nom1, T.unpack -> nom2)) = do
  let fadenDosuj = "data" </> T.unpack retej' </> "FORUM" </> nom1 </> nom2
  pure \revVoj tekst -> do
    let dosuj = foldr (flip (</>)) fadenDosuj (show <$> revVoj)
    createDirectoryIfMissing True dosuj
    dosUtf8 (dosuj </> "msg.txt") tekst

chrAlNum :: Char -> Maybe Int
chrAlNum = (`L.elemIndex` "0123456789")

dosUtf8 :: FilePath -> Text -> IO ()
dosUtf8 dos tekst =
  withFile dos WriteMode \h -> do
    hSetEncoding h utf8_bom
    hPutStr h tekst

disExt :: Text -> (Text, Text)
disExt d =
  let (dosNom', dosExt) = TP.breakOnEnd "." d
   in if
          | T.length dosExt `L.elem` [2, 3, 4],
            Just (dosNom, _) <- T.unsnoc dosNom',
            Nothing <- T.find (== '/') dosExt ->
              (dosNom, "." <> dosExt)
          | otherwise ->
              (d, "")

data Obj = Art' Text | Dos' Bool Text Text | Forum Text Text Int deriving (Show, Eq, Generic)

-- Mi min malamegas.
urlD :: Text -> Text
urlD = decodeUtf8Lenient . urlDecode False . encodeUtf8

kreiArt :: Text -> Obj
kreiArt = Art' . urlD . T.toLower

kreiDosUnc :: (Bool, Text, Text) -> Obj
kreiDosUnc (https, retej', nom) = Dos' https (urlD $ T.toLower retej') (urlD nom)

instance Hashable Obj

data StatusInfo = StatusInfo
  { sSukc :: Int,
    sFiask :: Int,
    sEntute :: Int
  }

data Status = Status
  { sInfo :: TVar StatusInfo, -- Sukcesitaj/Fiaskitaj/Entute
    sMutex :: TVar Bool,
    sFiaskitajList :: Handle
  }

data WikidotKonekt = WikidotKonekt {konektil :: !(Manager), token7 :: !ByteString, kukoj :: !(Text -> CookieJar)}

data Plan = Plan
  { -- | Atendvico de savendaj URL-oj.
    vic :: !(TQueue Obj),
    -- | Jam savitaj URL-oj.
    konataj :: !(S.Set Obj),
    mortnom :: !(TVar Int),
    konekt :: !WikidotKonekt,
    retej :: !Text,
    retejNomoj :: ![Text],
    loka :: Bool
  }

data SekuraIO a where
  SekuraIO :: IO a -> SekuraIO (Either Text a)

-- | Okaze de eraro reprovi @n@ fojojn.
sekuraIO :: Member SekuraIO efj => IO a -> Eff efj (Either Text a)
sekuraIO ago = send $ SekuraIO ago

sekuraIOTraktil :: SekuraIO a -> IO a
sekuraIOTraktil (SekuraIO ago) = catchAny (Right <$> ago) (pure . Left . tshow)

stm :: Member STM efj => STM a -> Eff efj a
stm = send

data StatusSignal a where
  SSciigi :: Text -> StatusSignal ()
  SPlan :: (StatusInfo -> Handle -> IO StatusInfo) -> StatusSignal ()

kunStatusTraktil ::
  forall r.
  FilePath ->
  ( ( forall efj a.
      (LastMember IO efj, Members '[STM] efj) =>
      Eff (StatusSignal ': efj) a ->
      Eff efj a
    ) ->
    IO r
  ) ->
  IO r
kunStatusTraktil = \voj kont -> do
  sInfo' <- newTVarIO $ StatusInfo 0 0 1
  sMutex' <- newTVarIO False
  withFile voj WriteMode \h -> kont (status' $ Status sInfo' sMutex' h)
  where
    status' (stat :: Status) = interpret \e -> do
      stm do
        sxlosita <- readTVar stat.sMutex
        if sxlosita
          then retry
          else writeTVar stat.sMutex True
      statOr <- stm $ readTVar stat.sInfo

      if statOr.sEntute /= 0
        then sendM $ cursorUp 1 >> clearLine
        else pure ()

      (rez0, statSkribota) <- case e of
        SSciigi tekst -> do
          sendM $ putStrLn tekst
          pure ((), statOr)
        SPlan gxisil -> do
          statNov <- sendM $ gxisil statOr stat.sFiaskitajList
          stm $
            writeTVar stat.sInfo $
              if statNov.sSukc + statNov.sFiask == statNov.sEntute
                then StatusInfo 0 0 0
                else statNov
          pure ((), statNov)

      sendM $ skrStatLin statSkribota

      stm $ writeTVar stat.sMutex False
      pure rez0
    skrStatLin :: StatusInfo -> IO ()
    skrStatLin (StatusInfo {sEntute = 0}) = pure ()
    skrStatLin stat = do
      (_, largx) <- fromMaybe (0, 70) <$> getTerminalSize
      let farT = tshow $ stat.sSukc + stat.sFiask
          entutT = tshow $ stat.sEntute
          montrilL = largx - T.length farT - T.length entutT - 4 - 3 -- [,], ,/
          sukcL = (stat.sSukc * montrilL) `div` stat.sEntute
          fiaskL = (stat.sFiask * montrilL) `div` stat.sEntute
          restL = montrilL - sukcL - fiaskL
          linio =
            "["
              <> T.pack (setSGRCode [SetColor Foreground Dull Red])
              <> T.replicate fiaskL "#"
              <> T.pack (setSGRCode [SetColor Foreground Dull Green])
              <> T.replicate sukcL "#"
              <> T.pack (setSGRCode [])
              <> T.replicate restL "-"
              <> "]"
              <> " "
              <> farT
              <> "/"
              <> entutT
      putStrLn linio

-- Plan, sciigi, stm, SekuraIO
planMedio ::
  (Monad IO, LastMember IO efj) =>
  Plan ->
  Eff (SekuraIO ': Reader Plan ': STM ': efj) a ->
  Eff efj a
planMedio plan =
  interpretM RIO.atomically -- Traktilo por STM
    . runReader plan -- Traktilo por plan
    . interpretM sekuraIOTraktil

$(makeEffect ''StatusSignal)
$(makeEffect ''Konservujo)
