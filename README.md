A simple tool for performing full backup of a Wikidot site.

The backup includes:
* All pages and all their versions accessible by the bot account.
* All linked files
* All forum threads (in an ugly, but working way)

This tool is incomplete and, most likely, will never be. You definitely can use, but I don't recommend you to.

If Wikidots' backup really doesn't satisfy you, maybe you should write to guardspirit@protonmail.com, he will help to make a full backup of your Wikidot site.

----

## Acquiring the executable

### With Nix package manager
```
$ nix build .
```
The result binary will be placed at result/wikidot-evakuilo.

## Running
`wikidot-evakuilo SITENAME [--proxy IP:PORT] [--threads=NUMBER]`

`credentials.txt` of format
```
wikidotUsername
password123
```
must be present in the working directory. The backup will also be stored in the working directory.
