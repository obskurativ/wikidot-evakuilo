{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/master";

  outputs = { self, nixpkgs, ... }:
    let 
      sis = "x86_64-linux";
      supportedSystems = [ "x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
    in {
      devShell.${sis} = with nixpkgs.legacyPackages.${sis};
        mkShell {
          nativeBuildInputs = [
            haskell-language-server
          ];
        };

      packages = forAllSystems (system: 
        with import nixpkgs { inherit system; }; rec {
          default = for.ghc923;
          for = nixpkgs.lib.mapAttrs 
            (_: v: v.callPackage ./cabal.nix {})
            pkgs.haskell.packages;
        }
      );
    };  
}
